package com.demo.falabella.soap.demo_soap.service.impl;

import com.demo.falabella.soap.demo_soap.calculatorws.Calculator;
import com.demo.falabella.soap.demo_soap.calculatorws.CalculatorSoap;
import com.demo.falabella.soap.demo_soap.dao.OperacionDao;
import com.demo.falabella.soap.demo_soap.dao.impl.OperacionDaoImpl;
import com.demo.falabella.soap.demo_soap.models.Operacion;
import com.demo.falabella.soap.demo_soap.service.OperacionService;

public class OperacionServiceImpl implements OperacionService {

	@Override
	public Operacion ejecutarOperacion(Operacion operacion) {
		// Se genera un objeto ws calculadora
		Calculator service = new Calculator();
		// Se implementa el ws en la interfaz CalculatorSoap
		CalculatorSoap calculatorService = service.getCalculatorSoap();
		Integer resultado = null;
		switch(operacion.getOperacion().name()) {
		case "SUMA":
			resultado = calculatorService.add(operacion.getValor1(), operacion.getValor2());
			break;
		case "RESTA":
			resultado = calculatorService.subtract(operacion.getValor1(), operacion.getValor2());
			break;
		case "DIVISION":
			resultado = calculatorService.divide(operacion.getValor1(), operacion.getValor2());
			break;
		case "MULTIPLICACION":
			resultado = calculatorService.multiply(operacion.getValor1(), operacion.getValor2());
			break;
		}
		operacion.setResultado(resultado);
		return operacion;
		}

	@Override
	public Boolean guardarOperacion(Operacion operacion) {
		OperacionDao operacionDao = new OperacionDaoImpl();
		return operacionDao.saveOperacion(operacion);
	}
}
