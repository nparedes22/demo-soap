package com.demo.falabella.soap.demo_soap;

import java.util.Scanner;

import com.demo.falabella.soap.demo_soap.config.operacionesCons;
import com.demo.falabella.soap.demo_soap.models.Operacion;
import com.demo.falabella.soap.demo_soap.service.OperacionService;
import com.demo.falabella.soap.demo_soap.service.impl.OperacionServiceImpl;;

public class App {
	public static void main(String[] args) throws Exception {

		Enum operacion = null;
		while (true) {
			do {
				operacion = interaccionUsuarioOperacion();
				if (operacion == null) {
					System.out.println("Por favor ingresar una operacion valida");
				}
			} while (operacion == null);

			if (operacion.name().toLowerCase().equals("cerrar")) {
				break;
			}

			Integer valor1 = null;
			do {
				valor1 = interaccionUsuarioValor(1);
				if (valor1 == null) {
					System.out.println("Por favor ingresar un valor valido de tipo entero");
				}
			} while (valor1 == null);

			Integer valor2 = null;
			do {
				valor2 = interaccionUsuarioValor(2);
				if (valor2 == null) {
					System.out.println("Por favor ingresar un valor valido de tipo entero");
				}
			} while (valor2 == null);

			Operacion operacionEjecutada = ejecutarOperacion(operacion, valor1, valor2);

			System.out.println("El resultado de la " + operacionEjecutada.getOperacion().name().toLowerCase() + " es: "
					+ operacionEjecutada.getResultado());

			System.out.println("");

			if (guardarOperacion(operacionEjecutada)) {
				System.out.println("Se ejecuto correctamente la insercion del registro operacion en base de datos");
			} else {
				System.out.println("Ocurrio un error al intentar guardar el registro en base de datos");
			}

			System.out.println("");

			Thread.sleep(3000);
		}
	}

	public static Enum interaccionUsuarioOperacion() {
		System.out.println("Ingresar una operacion a realizar por el ws de la siguiente lista:");
		System.out.println("- Sumar");
		System.out.println("- Restar");
		System.out.println("- Dividir");
		System.out.println("- Multiplicar");
		System.out.println("");
		System.out.println("(para cerrar el aplicativo utilizar la operacion \"cerrar\")");
		System.out.println("");
		System.out.print("operacion: ");
		Scanner scanner = new Scanner(System.in);
		String inputOperacion = scanner.nextLine();
		System.out.println("");
		return validarOperacion(inputOperacion);
	}

	public static Enum validarOperacion(String operacion) {
		Enum enumOperacion = null;
		switch (operacion) {
		case "sumar":
			enumOperacion = operacionesCons.SUMA;
			break;
		case "restar":
			enumOperacion = operacionesCons.RESTA;
			break;
		case "dividir":
			enumOperacion = operacionesCons.DIVISION;
			break;
		case "multiplicar":
			enumOperacion = operacionesCons.MULTIPLICACION;
			break;
		case "cerrar":
			enumOperacion = operacionesCons.CERRAR;
			break;

		default:
			return null;
		}
		return enumOperacion;
	}

	public static Integer interaccionUsuarioValor(int count) {
		System.out.println("Por favor integrar el valor " + count + " que se utilizara para consumir el WS:");
		System.out.println("");
		System.out.print("valor " + count + ": ");
		Scanner scanner = new Scanner(System.in);
		String inputOperacion = scanner.nextLine();
		System.out.println("");
		return ValidarValorValor(inputOperacion);
	}

	public static Integer ValidarValorValor(String valor) {
		Integer valorInt = null;
		try {
			valorInt = Integer.parseInt(valor);
		} catch (java.lang.NumberFormatException e) {
			return null;
		}
		return valorInt;
	}

	public static Operacion ejecutarOperacion(Enum operacionEnum, Integer valor1, Integer valor2) {
		Operacion operacion = new Operacion();
		operacion.setOperacion(operacionEnum);
		operacion.setValor1(valor1);
		operacion.setValor2(valor2);

		OperacionService operacionService = new OperacionServiceImpl();
		return operacionService.ejecutarOperacion(operacion);
	}

	public static Boolean guardarOperacion(Operacion operacion) {
		OperacionService operacionService = new OperacionServiceImpl();
		return operacionService.guardarOperacion(operacion);
	}
}
