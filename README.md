# demo-soap

Demo de ejemplo de consumo de WS soap e inserción de registro en base de datos DB2


## Herramientas utilizadas

Para generar las clases del consumo soap utilizar la herramienta wsimport (Está herramienta se encuentra dentro del directorio bin del jdk).

El comando para generar las clases es el siguiente:
```bash
wsimport -keep -p calculatorws http://www.dneonline.com/calculator.asmx?WSDL
```

## Instalación de la base de datos
Se utilizó db 2 como gestor de base de datos y el comando para levantar un contenedor docker de esta BD es el siguiente

```bash
sudo docker run -itd --name db2 --privileged=true -p 50001:50000 -e LICENSE=accept -e DB2INST1_PASSWORD=ACL.2019 -e DBNAME=testdb -v /home/nparedes/volumenes/db2:/database ibmcom/db2
```
La sentencia sql que se utilizo para generar la tabla es el siguiente:

```sql
CREATE TABLE OPERACIONES (
OPERACION VARCHAR(30),
VALOR1 INT,
VALOR2 INT, 
RESULTADO INT)
```
